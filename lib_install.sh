#!/bin/sh

APP_DIR="/usr/local/ai-cam"

echo "== Installing QT libraries in /usr/local"
sudo tar -xf Qt-5.15.1.tar.gz -C /usr/local
echo "== Installing libxcb in /usr/lib/aarch64-linux-gnu/"
sudo tar -xf libxcb.tar.gz -C /usr/lib/aarch64-linux-gnu/

if [ !  -d "$APP_DIR" ]; then
    mkdir $APP_DIR
fi

echo "== Installing models in $APP_DIR"
cp *.engine $APP_DIR
echo "== done =="
